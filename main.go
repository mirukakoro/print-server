package main

import (
	"fmt"
	"log"
	"net/smtp"
    "os"
)

func main() {
    log.Println("[info]  connect to smtp server")
    os.Setenv("CDEL_PRINT_SERVER_SMTP_SERVER", "smtp.gmail.com:587")
	c, err := smtp.Dial(os.Getenv("CDEL_PRINT_SERVER_SMTP_SERVER"))
	if err != nil {
		log.Fatal("[error] connect error:", err)
	}

    log.Println("[info]  set sender and recipient")
	if err := c.Mail(os.Getenv("CDEL_PRINT_SERVER_SENDER")); err != nil {
		log.Fatal("[error] set sender:", err)
	}
	if err := c.Rcpt(os.Getenv("CDEL_PRINT_SERVER_RECIPIENT")); err != nil {
		log.Fatal("[error] set recipient:", err)
	}

	log.Println("[info]  make email")
	wc, err := c.Data()
	if err != nil {
		log.Fatal("[error] make email:", err)
	}
	_, err = fmt.Fprintf(wc, "This is the email body")
	if err != nil {
		log.Fatal("[error] set email:", err)
	}
	err = wc.Close()
	if err != nil {
		log.Fatal("[error] close email data:", err)
	}

	log.Println("[info]  quit connection")
	err = c.Quit()
	if err != nil {
		log.Fatal("[error] quit:", err)
	}
}